# ITRparser #

ITRparser is a small program designed to extract prespecified categories from outlook and calculate how much time was spent on each category for a given day.

## Issue ##

Within the FDA we are required to track out time within a system called *Insight Time Reporting*. 

To be efficient one should update the applicatoin with your time on a daily basis. However, I generally find myself doing it at the end of the week, and looking through my outlook calendar to see how I spent my time, which is very time consuming and tedious.

By creating a prescribed list of ITR categories, and assigning all calendar events to a category this program is able to streamline the process by reading my outlook calendar and claculating how much time is spend in each category.  This information is used to *assist* in filing out ITR, and not meant to replace the need to carfully consider how one spends their time and report it accurately.

This program is of no value outside this very limited usecase, and contains nothing which would allow anyone to gather infromation about the FDA.

The code contains a sample list of ITR categories in a json file wich is used as a seed and intended to be edited by the user. Additionally the same exact categories must be created in Outlook

### prequirments: ###
- python
- pyside6
- pyinstaler - compile
