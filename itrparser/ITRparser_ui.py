# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ITRparser.ui'
##
## Created by: Qt User Interface Compiler version 6.2.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCalendarWidget, QDateEdit, QMainWindow,
    QPushButton, QSizePolicy, QTextBrowser, QWidget)
import itrparser.ITRparser_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(420, 546)
        icon = QIcon()
        icon.addFile(u":/icons/itricon.ico", QSize(), QIcon.Normal, QIcon.On)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.textBrowser = QTextBrowser(self.centralwidget)
        self.textBrowser.setObjectName(u"textBrowser")
        self.textBrowser.setGeometry(QRect(10, 40, 401, 501))
        self.fromDate = QDateEdit(self.centralwidget)
        self.fromDate.setObjectName(u"fromDate")
        self.fromDate.setGeometry(QRect(60, 10, 110, 22))
        self.toDate = QDateEdit(self.centralwidget)
        self.toDate.setObjectName(u"toDate")
        self.toDate.setGeometry(QRect(210, 10, 110, 22))
        self.parseButton = QPushButton(self.centralwidget)
        self.parseButton.setObjectName(u"parseButton")
        self.parseButton.setGeometry(QRect(330, 10, 75, 24))
        self.calWidget = QCalendarWidget(self.centralwidget)
        self.calWidget.setObjectName(u"calWidget")
        self.calWidget.setGeometry(QRect(10, 40, 392, 211))
        self.fromButton = QPushButton(self.centralwidget)
        self.fromButton.setObjectName(u"fromButton")
        self.fromButton.setGeometry(QRect(7, 10, 51, 21))
        self.toButton = QPushButton(self.centralwidget)
        self.toButton.setObjectName(u"toButton")
        self.toButton.setGeometry(QRect(176, 10, 31, 21))
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Outlook ITR parser", None))
        self.fromDate.setDisplayFormat(QCoreApplication.translate("MainWindow", u"M/dd/yyyy", None))
        self.toDate.setDisplayFormat(QCoreApplication.translate("MainWindow", u"M/d/yyyy", None))
        self.parseButton.setText(QCoreApplication.translate("MainWindow", u"Parse", None))
        self.fromButton.setText(QCoreApplication.translate("MainWindow", u"From:", None))
        self.toButton.setText(QCoreApplication.translate("MainWindow", u"To: ", None))
    # retranslateUi

