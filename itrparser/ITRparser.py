import sys
from pathlib import Path
import tomli
import shutil
import datetime
import traceback
from pprint import pprint
from functools import partial

from itrparser.ITRparser_ui import Ui_MainWindow

import itrparser.ITRparser_rc

from itrparser.spinner.waitingspinnerwidget import QtWaitingSpinner

from PySide6.QtWidgets import (
    QMainWindow,
    QApplication,
)

from PySide6.QtGui import (
    QColor,
    QAction,
)

from PySide6.QtCore import (
    QTimer,
    QRunnable,
    QThreadPool,
    Slot,
    Signal,
    QObject,
    QFile,
    QIODevice,
)

if sys.platform.startswith('win'):
    import win32com.client as wc

class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        tuple (exctype, value, traceback.format_exc() )

    result
        object data returned from processing, anything

    progress
        int indicating % progress

    '''
    finished = Signal()
    error = Signal(tuple)
    result = Signal(object)
    progress = Signal(int)

class Worker(QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    '''

    def __init__(self, fn, *args, **kwargs):
        super().__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        self.kwargs['progress_callback'] = self.signals.progress

    @Slot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''

        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


class IcalParcer(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.categories, self.itrModel = self.getCategories()
        self.tofrom = False
        self.setupUi(self)

        self.calWidget.hide()

        self.threadpool = QThreadPool()

        self.wspinner = self.setupSpinner()

        today = datetime.datetime.now()
        mon = today - datetime.timedelta(days = (today.isoweekday() - 1))
        fri = today + datetime.timedelta(days = (5 - today.isoweekday()))

        self.fromDate.setDate(mon)
        self.toDate.setDate(fri)

        self.fromButton.clicked.connect(self.fromCal)
        self.toButton.clicked.connect(self.toCal)
        self.calWidget.clicked.connect(partial(self.setDate))

        # self.parseButton.clicked.connect(self.parseCats)
        self.parseButton.clicked.connect(self.parseAction)

        # bind F1 to show a help screen
        self.myhelp = QAction('help')
        self.myhelp.setShortcut('f1')
        self.myhelp.triggered.connect(self.printhelp)

        self.addAction(self.myhelp)

    def printhelp(self):
        '''
        display a help message
        '''
        htmlmsg = QFile(':/html/help.html')
        # print(dir(msg))
        htmlmsg.open(QIODevice.ReadOnly | QIODevice.Text)
        msg = htmlmsg.readAll()
        htmlmsg.close()
        self.textBrowser.setHtml(msg.data().decode('utf-8'))

    def setupSpinner(self):
        s = QtWaitingSpinner(self)
        s.setNumberOfLines(10)
        s.setLineLength(25)
        s.setLineWidth(10)
        s.setInnerRadius(25)
        s.setColor(QColor('red'))
        return s

    def get_calendar(self, begin, end):
        '''
        return an outlook calander object rescricted to only those events bwtween the begin and enddates
        begin : date string
        end: date string
        '''
        outlook = wc.Dispatch('Outlook.Application').GetNamespace('MAPI')
        calendar = outlook.getDefaultFolder(9).Items
        calendar.IncludeRecurrences = True
        calendar.Sort('[Start]')
        restriction = f"[Start] > '{begin}' AND [END] < '{end}'"
        #restriction = "[Start] >= '" + begin.strftime('%m/%d/%Y') + "' AND [END] <= '" + end.strftime('%m/%d/%Y') + "'"
        calendar = [ cal for cal in calendar.Restrict(restriction)]
        return calendar

    def cal_time(self, e):
        '''
        given an event determin the time delta
        '''
        start = e.start
        end = e.end
        return end-start

    def parseCal(self, startDate, endDate):
        '''
        parse all the calandar events between the start and end dates
        '''
        days = dict()

        for e in self.get_calendar(startDate.strftime('%m/%d/%Y'), endDate.strftime('%m/%d/%Y')):
            ecats = self.categories.copy()
            day = e.start.date()
            dayf = f"{day.month}/{day.day}/{day.year}"

            if (startDate <= day <= endDate): # double check to make sure the data is actualy in range
                if day not in days:
                    days[day] = [dayf, ecats]
                    
                # print(f'{day} - {e.subject}')
                try:
                    mycats = e.categories
                except:
                    mycats = False
                if mycats:
                    # print(mycats)
                    for c in ecats.keys():
                        if c in mycats:
                            # day = e['DTSTART'].dt.day
                            # if day not in days:
                            #     days[day] = cats
                            days[day][1][c] += self.cal_time(e)
        return days

    def getCategories(self):
        '''
        Reads a json file of outlook-ITR categories and returns a category object 
        '''
        config = Path('AppData','Roaming') if sys.platform.startswith('win') else Path('.config')
        configfile = Path(Path().home(), config, 'ITRparser.toml')
        print(configfile)
        if not configfile.exists():
            shutil.copy(Path(Path(__file__).parent.resolve(), 'ITRparser.toml'), configfile)

        itrCodes = dict()

        with open(configfile, 'rb') as t:
            itr = tomli.load(t)
        
        for a in itr['activities']:
            for i in itr['activities'][a]:
                itrCodes[i['code']] = datetime.timedelta(0)
        
        return itrCodes, itr

    def fromCal(self):
        self.tofrom = 'From'
        self.calWidget.setSelectedDate(self.fromDate.date())
        self.calWidget.show()

    def toCal(self):
        self.tofrom = 'To'
        self.calWidget.setSelectedDate(self.toDate.date())
        self.calWidget.show()


    def setDate(self, d):
        if self.tofrom == 'From':
            w = self.fromDate
        elif self.tofrom == 'To':
            w = self.toDate
        self.tofrom = False
        self.calWidget.hide()
        w.setDate(d)

    def parseAction(self):
        print('starting spinner')
        self.wspinner.start()
        worker = Worker(self.parseCats)
        worker.signals.result.connect(self._parseAction)
        self.threadpool.start(worker)
        
    def _parseAction(self, s):
        self.textBrowser.setHtml(s)
        self.wspinner.stop()

    def parseCats(self, progress_callback):
        '''
        parse the calendare and figour out how long you've spent on any given clalendar catagory
        '''

        startDate = (datetime.date(day=self.fromDate.date().day(), 
            month=self.fromDate.date().month(), 
            year=self.fromDate.date().year()
            ) - datetime.timedelta(days = 1))

        endDate = (datetime.date(
            day = self.toDate.date().day(),
            month = self.toDate.date().month(),
            year = self.toDate.date().year()
        ) + datetime.timedelta(days = 1))

        data = self.parseCal(startDate, endDate)
        days = list(data.keys())
        days.sort()
        dataval = []
        print(self.itrModel['activities'])
        for day in days:
            dataval.append(f'<b>Date: {data[day][0]}</b><br>')
            dataval.append('<table cellpadding=2>')
            dataval.append(f'<tr><th>ITR</th><th>Time</th></tr>')
            for a in self.itrModel['activities']:
                dataval.append(f'<tr><th style="border-bottom: 3px solid black; border-top: 3px solid black;" align="left" colspan="2" >{a}</th></tr>')
                for i in self.itrModel['activities'][a]:
                    dataval.append('<tr>')
                    dataval.append(f'<td style="border-bottom: 1px dotted black;">L1: {i["l1"]} <br> L2: {i["l2"]} <br>type: {i["type"]}</td>')
                    dataval.append(f'<td style="border-bottom: 1px dotted black;">{data[day][1][i["code"]]}</td>')
                    dataval.append('</tr>')

            # for itr, val in data[day][1].items():
            #     dataval.append(f'<tr><td>{itr}</td><td>{val}</td></tr>')
            dataval.append('</table>')
            dataval.append('<hr>')

        # self.wspinner.stop()
        # self.textBrowser.setHtml(''.join(dataval))
        return ''.join(dataval)


def run():
    app = QApplication(sys.argv)

    window = IcalParcer()
    window.show()

    sys.exit(app.exec())


if __name__ == '__main__':
    run()
